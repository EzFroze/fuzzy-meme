﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            string h = "hello";
            Console.WriteLine("h = {0}", h);
            Console.WriteLine("h = {0}", h.Length);
            Console.WriteLine("h в верхунем регистре = {0}", h.ToUpper());
            Console.WriteLine("h в нижнем регистре = {0}", h.ToLower());
            Console.WriteLine("h содержит e? = {0}", h.Contains("e"));
            Console.WriteLine("Замена {0}", h.Replace("llo", "oll"));
        }
    }
}
