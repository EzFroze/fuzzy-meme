﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_world
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world");
            Console.WriteLine("d7 {0:d7}", 123456);
            Console.WriteLine("c: {0:c}", 12345);
            Console.WriteLine("f3: {0:f3}", 123456);
        }
    }
}
